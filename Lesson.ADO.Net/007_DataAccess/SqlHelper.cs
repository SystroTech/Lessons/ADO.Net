﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_DataAccess
{
    public static class SqlHelper
    {
        private const string connString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=MicDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Pooling=true";

        public static DataTable Execute(string query)
        {
            var dt = new DataTable();

            using (var connection = new SqlConnection(connString))
            {
                connection.Open();
                using (var adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = new SqlCommand(query, connection);
                    adapter.Fill(dt);
                }
            }

            return dt;
        }
    }
}