﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace _001_ConnectionStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source=.\SQLEXPRESS;Initial Catalog=Blogging; Integrated Security=True";

            SqlConnection connection = new SqlConnection(conStr);

            // adsa

            try
            {
                connection.Open(); // открытие физического подключения к источнику данных 
                Console.WriteLine(connection.State);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close(); //закрытие физического соединения с источником данных
                Console.WriteLine(connection.State);
            }
        }
    }
}
