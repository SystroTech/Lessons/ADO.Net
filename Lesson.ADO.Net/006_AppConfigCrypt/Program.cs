﻿using System;
using System.Configuration;

namespace _006_AppConfigCrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings("ConnectionStr1", @"Data Source=.\SQLEXPRESS;Initial Catalog=Blogging;Integrated Security=True;"));
            config.Save();

            ConnectionStringsSection section = config.GetSection("connectionStrings") as ConnectionStringsSection;

            if (section.SectionInformation.IsProtected)
            {
                // Расшифровать секцию
                section.SectionInformation.UnprotectSection();
            }
            else
            {
                // Зашифровать секцию.
                section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }

            // Сохранить файл конфигурации.
            config.Save();

            // Проверка шифрования
            Console.WriteLine("Protected={0}", section.SectionInformation.IsProtected);

            Console.WriteLine(ConfigurationManager.ConnectionStrings["ConnectionStr1"].ConnectionString);
        }
    }
}