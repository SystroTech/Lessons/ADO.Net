﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_SqlCommand
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=MicDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            SqlConnection connection = new SqlConnection(conStr);

            // Test Commit

            var dt = new DataTable();
            
            try
            {
                connection.Open();

                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = new SqlCommand("Select * from Student where CityId = 1", connection);
                    adapter.Fill(dt);
                }
                Console.WriteLine(connection.State);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close(); //закрытие физического соединения с источником данных
                Console.WriteLine(connection.State);
            }

            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    Console.Write(row[column.ColumnName]);
                    Console.Write("    ");
                }

                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}
