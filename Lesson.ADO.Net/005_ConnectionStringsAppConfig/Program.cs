﻿using System;
using System.Configuration;


namespace _005_ConnectionStringsAppConfig
{
    // https://msdn.microsoft.com/ru-ru/library/system.configuration.configurationmanager.connectionstrings(v=vs.110).aspx
    class Program
    {
        static void Main(string[] args)
        {
            //string bloggingConnectionString = ConfigurationManager.ConnectionStrings["BloggingConnection"].ConnectionString;

            // Объект ConnectionStringSettings представляет собой отдельную строку подключения в разделе строк подключения 
            // конфигурационного файла
            var setting = new ConnectionStringSettings
            {
                Name = "MyConnectionString1",     //имя строки подключения в конфигурационном файле
                ConnectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Blogging;Integrated Security=True;"
            };

            Configuration config;  // Объект Config представляет конфигурационный файл
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);  // Объект ConfigurationManager предоставляет доступ к файлам конфигурации
            config.ConnectionStrings.ConnectionStrings.Add(setting);
            config.Save();

            Console.WriteLine("Строка подключения записана в конфигурационный файл.");

            foreach (ConnectionStringSettings item in ConfigurationManager.ConnectionStrings)
            {

            }

            var myConnection = ConfigurationManager.ConnectionStrings["MyConnectionString1"];
            // Получение строки подключения.
            Console.WriteLine(myConnection.ConnectionString);
        }
    }
}