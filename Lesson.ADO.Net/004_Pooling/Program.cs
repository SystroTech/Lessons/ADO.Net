﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace _004_Pooling
{
    class Program
    {
        static void Main(string[] args)
        {
            // включение или отключение пула для этого подключения
            //string conStr = @"Data Source=.\SQLEXPRESS;Initial Catalog=Blogging; Integrated Security=true; Pooling=true";
            string conStr = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=MicDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Pooling=false";

            DateTime start = DateTime.Now;

            for (int i = 0; i < 1000; i++)
            {
                SqlConnection connection = new SqlConnection(conStr);
                // при включенном пуле физическое соединение не создается, а берется из пула соединений
                connection.Open();
                // при включенном пуле физическое соединение не разрывается, а помещается в пул
                connection.Close();
            }

            TimeSpan stop = DateTime.Now - start;

            Console.WriteLine(stop.TotalSeconds);

            Console.ReadLine();
        }
    }
}
